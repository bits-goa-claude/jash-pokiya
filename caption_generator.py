import anthropic
import base64
import httpx


with open("image    .jpeg", "rb") as image_file:
    image_data = image_file.read()
image_media_type = "image/jpeg"
image_data = base64.b64encode(image_data).decode("utf-8")
client = anthropic.Anthropic(
    api_key="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"
)
message = client.messages.create(
    model="claude-3-opus-20240229",
    max_tokens=1024,
    messages=[
        {
            "role": "user",
            "content": [
                {
                    "type": "image",
                    "source": {
                        "type": "base64",
                        "media_type": image_media_type,
                        "data": image_data,
                    },
                },
                {
                    "type": "text",
                    "text": "Generate a small captivating caption for this image along with trendy hashtags such that it is relevant for posting on instagram and other social media."
                }
            ],
        }
    ],
)

caption = message.content[0].text
print(caption)